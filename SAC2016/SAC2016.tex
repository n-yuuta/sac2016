\documentclass{sig-alternate-05-2015}
  \pdfpagewidth=8.5truein
  \pdfpageheight=11truein
\usepackage{multirow}
\usepackage{url}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{subfigure}
\begin{document}
% Copyright
%\setcopyright{acmcopyright}

\title{Preprocessor-aware Detection of Coding Patterns \\in C Programs}

%\numberofauthors{5}

%\author{
%\begin{tabular}{cc}
%\multicolumn{2}{l}{Yuta Nakamura$^\dagger$, Enjong Choi$^\dagger$, Norihiro Yoshida$^\dagger$$^\dagger$, Syusuke Haruna$^\dagger$ and Katsuro Inoue$^\dagger$\vspace{0.3cm}} \\
%\affaddr{$^\dagger$Osaka University, Japan} & \affaddr{$^\dagger$$^\dagger$Nagoya University, Japan}\\
%\affaddr{\{n-yuuta,haruna,inoue\}@ist.osaka-u.ac.jp} & \affaddr{yoshida@ertl.jp}\\
%\affaddr{ejchoi@osipp.osaka-u.ac.jp} & \\
%\end{tabular}
%}

\maketitle

\begin{abstract}
Large-scale source code usually follows a lot of coding patterns. For example, fopen() should be followed by a call fclose() later. Besides such a well-known coding pattern, many implicit patterns appear in large-scale source code. When programmers are unaware of them, defects can be easily introduced. The existing approaches are only of limited use for real-time and/or resource-limited domains because of the unawareness of preprocessor (i.e., macro) instructions. In this paper, we propose a preprocessor-aware approach to detect coding patterns in C programs. The case study shows that the proposed approach successfully detects coding patterns that correspond to the specifications of a real time embedded OS.
\end{abstract}

\begin{CCSXML}
<ccs2012>
<concept>
<concept_id>10011007.10011074.10011111.10011696</concept_id>
<concept_desc>Software and its engineering~Maintaining software</concept_desc>
<concept_significance>500</concept_significance>
</concept>
</ccs2012>
\end{CCSXML}

\ccsdesc[500]{Software and its engineering~Maintaining software}

%
%  Use this command to print the description
%
\printccsdesc

\keywords{Coding pattern, Pattern mining, Preprocessor instruction}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                   INTRODUCTION                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{INTRODUCTION}
A coding pattern is a sequence of code fragments that frequently appears in source code \cite{Ishio:2008}. 
Large-scale source code usually follows a lot of coding patterns \cite{Li:2005}. 
For a simple example of a coding rule is a sequence of function calls $ \left(fopen, fclose \right)$: $fopen$ should be followed by a call $fclose$ later.
Besides such a well-known and documented coding pattern, many implicit patterns appear in large-scale source code, most of which are tedious to be documented by programmers \cite{Li:2005}.
When programmers are unaware of them, defects can be easily introduced \cite{better, Li:2005, Nguyen:2009, Wasylkowski:2007}.

So far, much research have been done on the automated detection of implicit coding patterns in large-scale source code \cite{better, Li:2005, Nguyen:2009, Wasylkowski:2007}.
The existing approaches make a list of key elements (e.g., functions calls) from each function declaration in source code at first, and then detect frequently appearing sequences/itemsets among the function declarations as coding patterns.

Real-time and/or resource-limited domains (e.g., embedded system development) should be prioritized for the detection of coding patterns.
In the domains, source code is typically written by C language and tends to be low modularity in order to keep responsiveness and reduce memory consumption.
It leads a lot of not only repeated parts but also instances of coding patterns in the source code.

The existing approaches are only of limited use for real-time and/or resource-limited domains because of the unawareness of preprocessor (i.e., macro) instructions.
In the domains, preprocessor instructions are frequently used in order to satisfy both source code variability and the responsiveness of a system \cite{EMBED}. 
Although a straightforward approach is detecting coding patterns from preprocessor-generated code, that approach is unable to detect coding patterns including preprocessor instructions.
Preprocessor instructions should be included in detected coding patterns because programmers write not preprocessor-generated code but preprocessor instructions directly.

In addition to preprocessor instructions, goto statement and label for it are frequently used in order to keep responsiveness of a real-time and/or resource-limited system.

In this paper, we propose a preprocessor-aware approach to detect coding patterns in C programs. 
The proposed approach is able to detect coding patterns including not only preprocessor instructions but also jump statements and label for them in source code. 
In the case study, we applied the proposed approach to a real-time OS for embedded systems.
The result shows that the proposed approach successfully detected coding patterns that correspond to the specifications of the real-time OS.






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%          SEQUENTIAL PATTERN MINING            %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{SEQUENTIAL PATTERN MINING}
Sequential pattern mining approach discovers frequent subsequences as patterns in a sequence database \cite{Agrawal1}.
It is commonly used in data mining for applications such as discovering a set of frequently visited web pages \cite{WEB2}. 

\textbf{SPADE (Sequential PAttern Discovery using Equivalence classes)} algorithm devised by Zaki is one of the sequential pattern mining algorithms \cite{SPADEalgorithm}. 
It takes the user-defined minimum support (i.e., a number of frequencies in the database) and the sequence database as parameter.
A sequence is an ordered list of items. 
Each sequence in the database has an unique identifier called Sequence ID (\textit{SID}).
A unique identifier for each item in a given sequence is called Event ID (\textit{EID}). 

Given a database of sequence and the minimum support, the SPADE finds all frequent subsequences whose support values are higher than minimum support in the database. 
Consider the input database for SPADE is Table \ref{database}(a) and the minimum support is 2. 
The table has four items (\textit{A} to \textit{D}), four sequences, and ten events in all.
To find frequent subsequences, SPADE starts with computing frequent 1-subsequences by scanning database and computing support of each single item. 
From the table \ref{database}(a), items \textit{A} and \textit{B} are frequent 1-subsequences because they appear more than two times as shown in table \ref{database}(b).
Next, the SPADE generates frequent subsequences by repeatedly joining the subsequences. 
In detail, a pair of frequent subsequences are joined only if they satisfy three conditions, which (1) they have the same 
\textit{SID} (2) their \textit{EID}s are sorted in the ascending order (3) the support value of the joined subsequence is higher than the given minimum support.
Table \ref{database}(c) shows a result of joining frequent 1-subsequences, (\textit{A}) and (\textit{B}), for frequent 2-subsequences.
As shown in the table \ref{database}(b), subsequences (\textit{A}) and (\textit{B}) satisfy above-mentioned three conditions. 
Therefore, they are joined into frequent 2-subsequences, (\textit{A B}),  (\textit{A A}), and (\textit{B A}). 
As a result, these subsequences are discovered as a new frequent subsequence because they appear in the database more than two times. 



\begin{table}[hbt]
\caption{Example of Input Database for SPADE Algorithm (The Minimum Support is 2)}
\centering
\subfigure[Database]{
	\begin{tabular}{|c|c|c|}
	\hline
	SID & EID & Item \\
	\hline
	\hline
	1 & 10 & D \\
	\hline
	1 & 15 & A \\
	\hline
	1 & 20 & B \\
	\hline
	1 & 25 & A \\
	\hline
	2 & 15 & B \\ 
	\hline
	2 & 20 & A \\ 
	\hline
	3 & 10 & C \\ 
	\hline
	4 & 10 & A \\ 
	\hline
	4 & 20 & B \\ 
	\hline
	4 & 25 & A \\ 
	\hline
	\end{tabular}
}\\
\subfigure[1-subsequences]{
	\begin{tabular}{|c|c|c|c|}
	\hline
	Subsequence & Sup & SID & EID  \\
	\hline
	\hline
	\multirow{5}{*}{(A)} & \multirow{5}{*}{3} & 1 & 15 \\
	\cline{3-4}
	 & & 1 & 25 \\
	\cline{3-4}
	 & & 2 & 20 \\
	\cline{3-4}
	 & & 4 & 10 \\
	\cline{3-4}
	 & & 4 & 25 \\
	\hline
	\multirow{3}{*}{(B)} & \multirow{3}{*}{3} & 1 & 20 \\
	\cline{3-4}
	 & & 2 & 15 \\
	\cline{3-4}
	 & & 4 & 20 \\
	\hline
	\end{tabular}
}\\
\subfigure[2-subsequences]{
	\begin{tabular}{|c|c|c|c|}
	\hline
	Subsequence & Sup & SID & EID  \\
	\hline
	\hline
	\multirow{2}{*}{(A B)} & \multirow{2}{*}{2} & 1 & 20 \\
	\cline{3-4}
	 & & 4 & 20 \\
	\hline
	\multirow{2}{*}{(A A)} & \multirow{2}{*}{2} & 1 & 25 \\
	\cline{3-4}
	 & & 4 & 25 \\
	\hline
	\multirow{3}{*}{(B A)} & \multirow{3}{*}{3} & 1 & 25 \\
	\cline{3-4}
	 & & 2 & 20 \\
	\cline{3-4}
	 & & 4 & 25 \\
	\hline
	\end{tabular}
}
\label{database}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              PROPOSED APPROACH                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{PROPOSED APPROACH}
Our mining approach detects coding patterns from real-time and/or resource-limited systems written in \textit{C} language. 
The proposed approach takes \textit{C} programs as input and then outputs frequent codings patterns that appear in source code of the input programs.
Figure \ref{design} shows an overview of the proposed approach. 
The approach consists of three steps: 
(1) extracting elements; 
(2) detecting coding patterns from the elements; and 
(3) filtering and merging the coding patterns. 
The details of these steps will be explained in the following subsections.

\begin{figure}[!t]
\begin{center}
\includegraphics[scale=0.5]{./fig00.eps}
\caption{An Overview of the Proposed Approach\label{design}}
\end{center}
\end{figure}
%
%
\subsection{Extracting Elements}
%\subsection{EXTRACTING ELEMENTS}
%This step extracts sequences of elements from the input programs. 
In this step, we extracted function call, and control statements from source code as elements. 
Furthermore, we extracted macro and \textit{goto} and labeled statements because embedded programs frequently use \textit{goto} and labeled statements to keep responsiveness of a real-time and/or resource-limited system.
The extracted elements are explained as follows:
%
%
\begin{description}
\item[Function Call elements: ]
We extracted function names from function call statements. 
To accomplish this, parameters of the function call statements were excluded because we regarded that it is sufficient to understand the behavior of program with only function names.
In particular, C programs contain preprocessor-generated code. 
From these code, we only extracted macro names because we regarded that macro name is enough to express role of macros in the program.

\item[IF/ELSE/END-IF elements: ]
We extracted IF/ELSE/\\END-IF elements from \textsf{if} statements. 
The extracted elements from \textsf{if} statements consist of \textit{IF}, \textit{ELSE} and \textit{END-IF}. 
Figure \ref{conditional}(a) shows a rule for extracting these elements from \textsf{if} statements.
As you can see in this figure, when conditional expression calls a function, we place a name of the called function before the \textit{IF} element because \textsf{if} statements are branched by conditional expression. 
Moreover, elements controlled by the \textsf{if} statements are placed between \textit{IF} and its corresponding \textit{END-IF}.  


\item[LOOP/END-LOOP elements: ]
We extracted \textit{LOOP} and it corresponding \textit{END-LOOP} from \textsf{while} or \textsf{for} statements. 
Figure \ref{conditional}(b) and (c) shows a rule for extracting a pair of \textit{LOOP} and \textit{END-LOOP} elements from \textsf{while} or \textsf{for} statements, respectively.
Functions calls in conditional expressions in the \textsf{while} statement, and initialization, update, and condition expressions in the \textsf{for} statements are placed according to the order of execution. 
In particular, for \textsf{do-while} statements, we regarded them as \textsf{while} statements which are executed at least once regardless of the conditional expression. 
Therefore, same elements extracted from \textsf{do-while} statements as one that were extracted one from \textsf{while} statements except for excluding the first conditional element.


%2.3 ????????????C?????v???O???????W?????v????????p????????{??????
%??C????????????W?????v????????goto ?????v?f???????o???邱???????D????Cgoto
%????P????????????C?W?????v??????x??????????????????????x???????v?f???
%??????o?????D

\item[Goto/Label/Return elements: ]
We extracted each \textit{goto} statement and label for it as elements.
Generally, it is difficult for developers to understand the execution of program because \textit{goto} statements transfers execution control to some other part of the program \cite{Dijkstra}. 
In preference to \textit{goto} statements, a developer uses \textit{break}, \textit{continue}, and \textit{return} statements to make breakpoints of these statements clearly. 
Therefore, we also extracted these statements. 
%and labeled statements that are contained in these statements such as labeled statements exist between \textsf{\#ifdef} and %\textsf{\#endif}.
\end{description}

\begin{figure}[!t]
\begin{center}
\includegraphics[scale=0.47]{./fig11.eps}
\caption{Rules for Extraction from The Control Structure\label{conditional}}
\end{center}
\end{figure}

\subsection{Detecting Coding Patterns}
After extracting sequences of elements from the input program, the next step was to discover frequent coding patterns.
In order to extract frequent coding patterns, we used the SPADE, the sequential pattern mining algorithm, discussed in Section 2.  
We selected sequential pattern mining because it is better than other algorithms in terms of detecting faults \cite{better}.
In this step, at first, we generated the sequence database from the elements extracted in the previous step. 
%Each extracted element is corresponding to the item composing the sequence. 
Next, we applied the SPADE algorithm to the database and then found out coding patterns that appear frequently than minimum support. 
As the minimum support, we set up ten because we regarded those coding patterns that appear less than ten might be insufficient for determining defaults. 

\subsection{Filtering and Merging Coding Patterns }
In this step, we filtered out the detected coding patterns and then merged them into the maximal coding patterns. 
The coding patterns detected from the previous step might contain insignificant coding patterns.
To filter these coding patterns, we excluded the coding patterns if they satisfy one of the following three conditions.

\begin{itemize}
\item \textbf{Condition of Filter 1. The Number of Function Calls}\\
Coding patterns that contain less than one function call were excluded because we regarded coding patterns that contain a few function calls are insignificant. 
%
%
\item \textbf{Condition of Filter 2. The Correspondence of Elements Extracted from Control Statements}\\
We regarded coding patterns that contain a \textit{IF} or \textit{LOOP} element must contain the corresponding \textit{END-IF} or \textit{END-LOOP} element. Therefore, coding patterns that were incompatible with this correspondence were excluded.

\item \textbf{Condition of Filter 3. The Correspondence of Elements Extracted from Goto Statements}\\
We regarded coding patterns that contain \textsf{goto} element must contain correspondence label element and vice versa.
Therefore, coding patterns with incompatible goto and label elements were excluded.
To filter out these elements, we checked each codding pattern that contains \textsf{goto} elements and then counted the number of \textsf{goto} and label elements in it. 
Coding patterns that contain different number of \textsf{goto} and label elements were excluded. 
Moreover, when a function calls a macro that do not exist in the same file and the macro contains a \textsf{goto} statement, it is impossible for our approach to determine whether the coding pattern has the incomplete correspondence. 
In this case, we checked each header file in the same directory whether it contain the macro. If so, we counted the number of \textsf{goto} and label elements in the macro. 
% our approach considers the case where label statements exist between \textsf{\#ifdef} and \textsf{\#endif}.

\end{itemize}


After filtering out the insignificant coding patterns, we merged the coding patterns into the maximal pattern.
To elaborate, we merged the coding patterns only if they have the inclusion relation and their support values are the same.
%In detail, the pattern mining with the SPADE algorithm detects all the sequential patterns and its subsequences.
For example, the coding patterns (\textit{A}), (\textit{B}), (\textit{C}), (\textit{A B}), (\textit{B C}) and (\textit{A C}) are merged into (\textit{A B C}). 
%
%
%\begin{itemize}
%\item \textbf{Maximization}\\
%\hspace*{0.1in}
%The pattern mining with the SPADE algorithm detects all the sequential patterns and its subsequences.
%For example, when the sequential pattern (\textit{A B C}) is detected, its subsequences (\textit{A}), (\textit{B}), 
%(\textit{C}), (\textit{A B}), (\textit{B C}) and (\textit{A C}) are also detected. 
%Therefore, after filtering on the above-mentioned conditions, we merge coding patterns into a coding pattern only if they %have the inclusion relation and their support values are the same.
%\end{itemize}

\begin{table}[hbt]
\caption{Details of Subject Projects\label{subject projects}}
\begin{center}
\scalebox{1.0}{
\begin{tabular}{|l|r|r|r|}
\hline
Projects & \#C files & \#Functions & LOC \\
\hline
\hline
ATK2/SC1 & 12 & 81 & 4,620 \\
\hline
ATK2/SC1-MC & 17 & 131 & 7,726 \\
\hline
ATK2/SC3 & 16 & 135 & 7,698 \\
\hline
ATK2/SC3-MC & 19 & 172 & 10,244 \\
\hline
\end{tabular}
}
\end{center}
\end{table}

\begin{table*}[hbt]
\caption{\label{filtering}The Number of Detected And Filtered Coding Patterns}
\begin{center}
\scalebox{1.0}{
\begin{tabular}{|l|r|r|r|r|r|r|}
\hline
Projects & \#Detected  & \#Filter 1 & \#Filter 2 & \#Filter 3 & \#Mergence & Elimination(\%) \\
 & Coding Patterns & & & & & \\
\hline
\hline
ATK2/SC1 & 408,613 & 392,648 & 258,441 & 9,026 & 29 & 99.993 \\
\hline
ATK2/SC3 & 157,148 & 150,259 & 97,909 & 3,639 & 64 & 99.959 \\
\hline
ATK2/SC1-MC & 17,561,490 & 17,499,108 & 17,204,504 & 154,253 & 240 & 99.999 \\
\hline
ATK2/SC3-MC & 34,279,185 & 34,246,413 & 34,138,960 & 274,025 & 511 & 99.999 \\
\hline
\end{tabular}
}
\end{center}
\end{table*}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              CASE STUDY                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{CASE STUDY}
We applied our approach to real-time OS and confirmed the usefulness of detected coding patterns based on the specifications. 
The goal of this case study is to confirm usefulness of the proposed approach in checking defects from source code.
As subject system, we selected four projects from \textsf{ATK2} (Automotive Kernel Version 2 series), a real-time operating system for automotive system control \footnote{https://www.toppers.jp/en/atk2.html}.
ATK2 is comprised of several projects. 
Among them, we selected \textit{SC1}, \textit{SC3}, \textit{SC1-MC}, and \textit{SC3-MC} projects because the \textit{SC1} is the basic project and the others are extended projects of it. 
Table \ref{subject projects} shows the details of the subject systems. 
From each project, we only selected \textit{C} files in the kernel directory.

\subsection{Detected Coding Patterns}
Table \ref{filtering} shows the number of coding patterns detected by our approach. 
In this table, the column \textsf{\# Detected Coding Patterns} indicates the number of coding patterns detected from each projects. 
The column \textsf{\# Filter 1}, \textsf{\# Filter 2}, and \textsf{\# Filter 3} shows the number of coding patterns after filtering by each condition in the Step 3, respectively.
The column \textsf{\# Mergence} shows the number of coding patterns after merging and \textsf{Elimination(\%)} represents the ratio of overall excluded coding patterns by filtering and merging  in the Step 3.
As you can see in this table, lots of coding patters were detected, which might contain many insignificant coding patterns. 
Furthermore, it is tedious for programmers to manually check all of them.
However, 99\% of coding patterns were eliminated by filtering and merging the coding patterns, as seen the in \textsf{Elimination(\%)} column in table \ref{filtering},  which implies that our filtering and mering make straightforward for manual check of the coding patterns.



\begin{table*}[hbt]
\caption{Example of Detected Coding Patterns\label{example of detected coding patterns}}
\begin{center}
\begin{tabular}{|c|c|l|}
\hline
Sup & Len&  Coding Pattern  \\
\hline
\hline
34 & 2 & x\_nested\_lock\_os\_int() / x\_nested\_unlock\_os\_int()\\
\hline
14 & 13 &CHECK\_DISABLEDINT() / CHECK\_CALLEVEL() / CHECK\_ID() / x\_nested\_lock\_os\_int() / \\ %EOL
& &  d\_exit\_no\_errorhook: / x\_nested\_unlock\_os\_int() / exit\_no\_errorhook: /return / exit\_errorhook: / \\ %EOL
& & x\_nested\_lock\_os\_int() / call\_errorhook() / goto / d\_exit\_error\_hook \\ %EOL
\hline
14 & 16 & CHECK\_DISABLEDINT() / CHECK\_CALLEVEL() / CHECK\_ID() /  CHECK\_RIGHT() /\\ %EOL
& & x\_nested\_lock\_os\_int() / D\_CHECK\_ACCESS() /  d\_exit\_no\_errorhook: / x\_nested\_unlock\_os\_int() /\\ %EOL
& & exit\_no\_errorhook: / return /exit\_errorhook: / x\_nested\_lock\_os\_int() / d\_exit\_errorhook: / \\ %EOL
& & call\_errorhook() / goto / d\_exit\_error\_hook \\ %EOL
\hline
11 & 18 & CHECK\_DISABLEDINT() / CHECK\_CALLEVEL() / CHECK\_ID() / CHECK\_CORE() /  \\ %EOL
& & x\_nested\_lock\_os\_int() / x\_nested\_unlock\_os\_int() /  exit\_no\_errorhook: /return / goto / \\ %EOL
& & errorhook\_start / exit\_errorhook: /  x\_nested\_lock\_os\_int() / errorhook\_start: /get\_my\_p\_ccb() /\\ %EOL
& &  call\_errorhook() / x\_nested\_unlock\_os\_int() / goto / exit\_no\_error\_hook \\ %EOL
\hline
11 & 23 & CHECK\_DISABLEDINT() / CHECK\_CALLEVEL() / CHECK\_ID() /  CHECK\_CORE() / \\ %EOL
& & CHECK\_RIGHT() / get\_p\_ccb() / x\_nested\_lock\_os\_int() / acquire\_cnt\_lock() / release\_cnt\_lock() /\\ %EOL
& &  x\_nested\_unlock\_os\_int() / exit\_no\_errorhook: / return /release\_cnt\_lock() / goto /  \\ %EOL
& & errorhook\_start / exit\_errorhook: / x\_nested\_lock\_os\_int() / errorhook\_start: /get\_my\_p\_ccb() /  \\ %EOL
& &call\_errorhook() / x\_nested\_unlock\_os\_int() / goto / exit\_no\_error\_hook \\ %EOL
\hline
\end{tabular}
\end{center}
\end{table*}

\subsection{Validation of Detected Coding Patterns}
To validate the coding patterns detected by our proposed approach, we selected 40 coding patterns and then manually checked them based on the specifications.
In detail, at first, we selected the top five of longest coding patterns as well as coding patterns with top five highest support values from the detected coding patterns. Overall, 40 coding patterns were selected. 
Next, we determined whether each coding pattern is sufficient for checking defects based on the specifications of \textit{ATK2}. We used specifications because we believe that the coding patterns that imply specifications, can be used for checking defects from source code.

As a result of validation, we found  25 coding patterns that are sufficient for checking defects. 
Table \ref{example of detected coding patterns} shows examples of the coding patterns that are determined as sufficient.
The column \textsf{Sup} and \textsf{Len} indicates the support value and length of the coding pattern, respectively. 
The column \textsf{Coding Pattern} shows each coding pattern that are separated into each element by \textit{slash} (/) marks. 
All coding patterns shown in this table, contain \textsf{x\_nested\_lock\_os\_int()} and \textsf{x\_nested\_unlock\_os\_int()} elements. 
These elements correspond to ``a pair of \textit{disabling} and \textit{enabling} functions should be called before executing system services (e.g., control alarms, tasks, and counters)'' in the specifications. 
%
%Moreover, \textit{ATK2} projects must check an error in the function of system service. 
%If errors occur, they do the corresponding error handling. 
%This specification can be seen in the coding pattern of the second row. 
%This firstly do the error check by \textsf{CHECK\_DISABLEDINT()}, \textsf{CHECK\_CALLEVEL()} and \\\textsf{CHECK\_ID()}, and they do the error handling by the corresponding labels, \textsf{exit\_no\_errorhook:} and \textsf{exit\_errorhook:}. 
%
Moreover, most of the coding patterns contain elements for error checking and error handling. 
A sequence of \textsf{CHECK\_DISABLEDINT()}, \textsf{CHECK\_CALLEVEL()}, and \textsf{CHECK\_ID()} elements corresponds to ``an error occurred in the function of system service must be checked'' in the specification. 
Moreover, a sequence of \textsf{exit\_no\_errorhook:} and \textsf{exit\_errorhook:} elements, that are for error handling, correspond to ``When the errors occur, correspondence error handling should be executed.'' in the specification. 
%
Other coding patterns, which were determined as insufficient, contain incomplete elements such as coding patterns contain only a single \textsf{x\_nested\_lock\_os\_int()} or \textsf{x\_nested\_unlock\_os\_int()} element.

%In summary, we concluded that we could detect the usefulness coding patterns and they can be used for 
%detecting bugs.

\subsection{Discussion}
In this section, we discussed on extracting coding patterns from real-time and/or resource-limited system,  performance of proposed approach, and detection of the similar coding patterns.

\begin{itemize}
\item \textbf{For Extracting Coding Patterns from Real-time and/or Resource-limited System}\\
To extract coding patterns from real-time and/or resource-limited system, we extracted not only macro  but also goto/label/return elements.
As a results, several coding patterns that contain \textsf{goto} and label elements were detected such as coding patterns shown in the Table \ref{example of detected coding patterns}. 
%These coding patterns can be detected by extracting jump statement elements.
%Therefore, we can say that extracting jump instructions are effective. 
%
%また，各CHECK関数はマクロ関数でありその内部でgoto文を呼び出している．このgoto文のジャンプ先のラベルは
%\#ifdef命令によって条件分岐している．手法ステップ3ではラベル文が\#ifdef命令に影響を受けている場合を
%考慮してラベルの対応関係による絞り込みを行っているため，プリプロセッサ命令を無視することはできないと考えられる．
Moreover, coding patterns that contain macros for checking errors, such as \textsf{CHECK\_ID()} in the table \ref{example of detected coding patterns} were detected. 
These macros contain a goto statement, which transfers control of execution to labeled statements. 
%These labeled statements are branched by \textsf{\#ifdef} and \textsf{\#endif}.  
These coding patterns can be detected by checking the correspondence of elements extracted from goto statements.


\item \textbf{Performance}\\
The case study was performed on a 64 bit Windows 7 Professional workstation equipped with 2 processor, 2.90GHz CPUs and 256GB of main memory.
The total detection time of coding patterns and time needed to complete the each step are shown in table \ref{detectionTime}. 
%The case study is conducted on execution environment is 
%as follows: CPU: Intel(R) Xeon(R) CPU E5-2690@2.90GHz 2.90GHz, RAM: 256GB.\\
As you can see in this table, the total detection times were varied for each subject project. 
It takes only few seconds to detect coding patterns in the \textit{ATK2/SC1} and \textit{ATK2/SC3} projects.  
On the other hand, it takes more than four hours in the \textit{ATK2/SC1-MC} and \textit{ATK2/SC3-MC} projects. 
We assume that this is caused by time to complete Step 2 and number of detected coding patterns. 
For instance, detecting time for Step 2 were about 3 to 81 seconds in the \textit{ATK2/SC1} and \textit{ATK2/SC3} projects whereas, 17,000 to 40,000  seconds in the \textit{ATK2/SC1-MC} and \textit{ATK2/SC3-MC} projects. 
Moreover, \textit{ATK2/SC1-MC} and \textit{ATK2/SC3-MC} projects contain more coding patterns than others, as you can see in the Table \ref{filtering}. 
This means that the detection time strongly depends on the number of detected coding patterns. 
In order to decrease the detection time for these programs that contain a large number of coding patterns, 
we plan to parallelizing the pattern mining is effective.
%
%
\item \textbf{Similar Coding Patterns}\\
Several detected coding patterns contains duplicated sequence of elements. 
For example, some of detected coding pattern are similar to the coding patterns in the Table \ref{example of detected coding patterns} except for \textsf{CHECK\_ID()}  or \\\textsf{CHECK\_DISABLEDINT()} element. 
All of these coding patterns represent the same error check and its handling. 
Therefore, they should be detected as one coding pattern. 
To merge this similar coding patterns, we plan to use the approach extracting 
redundancy-aware top-k patterns devised by Xin et al \cite{redundancy}. 
%This is for the item set mining, however we assume that it is effective for the sequential pattern mining.
\end{itemize}



\begin{table}[hbt]
\caption{Detection time in seconds\label{detectionTime}}
\begin{center}
\scalebox{0.9}{
\begin{tabular}{|l|r|r|r|r|}
\hline
 & Step 1 & Step 2 & Step 3 & {Total Time} \\
\hline
\hline
ATK2/SC1    & 0.094 & 3.1 & 2.1 & 5.3 \\
\hline
ATK2/SC3    & 0.078 & 81.0 & 1.1 & 83.0 \\
\hline
ATK2/SC1-MC & 0.108 & 17,160.0 & 74.0 & 17,220.0 \\
\hline
ATK2/SC3-MC & 0.078 & 39,660.0 & 148.0 & 39,780.0 \\
\hline
\end{tabular}
}
\end{center}
\end{table}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              RELATED WORK               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{RELATED WORK}
\input{relatedwork.tex}

\section{CONCLUSION and FUTURE WORK}
In this paper, we proposed a preprocessor-aware approach to detect coding patterns in C programs. 
The proposed approach is able to detect coding patterns including not only preprocessor instructions but also jump statements and label for them in source code. 
In the case study, we applied it to an real time embedded OS \textit{ATK2} and then assessed 
the usefulness of detected coding patterns. As a result, 25 of 40 coding patterns correspond the specifications.
We conclude that programmers have to be aware of them as rules. 

In future work, we plan to apply our approach to various programs. We applied it to only real-time 
operating system programs for the automotive control in this paper. However, we need to apply our approach to others to confirm the validation of it.
%\hspace*{0.1in}Also, we plan to use another approach. In this paper, we use the pattern mining approach and the SPADE 
%algorithm for detecting coding patterns. We plan to use another pattern mining algorithm or another approach 
%and compare our approach with them. We are especially interested in the program dependence graph approach.

%\section{ACKNOWLEDGMENT}
%\hspace*{0.1in}This work was supported by JSPS KAKENHI Grant Numbers 25220003 and 26730036.
\bibliographystyle{abbrv}
\bibliography{reference}

\balancecolumns

\end{document}

